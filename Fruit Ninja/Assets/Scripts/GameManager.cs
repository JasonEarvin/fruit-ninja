using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace FruitNinja
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PausePopupController pausePopup;
        [SerializeField] private GameOverPopupController gameOverPopup;
        [SerializeField] private FruitScript fruit;
        [SerializeField] private BombScript bomb;
        [SerializeField] private Spawner spawner;
        private GameState currentState = GameState.None;
        public Text scoreText;
        private bool isPaused;
        private int gainedFruit = 0;
        private int health = 1;

        public event Action GameStarted;
        public event Action<int> HealthChanged;
        public event Action<int> FruitChanged;

        // Start is called before the first frame update
        private void Awake()
        {
            pausePopup.ContinuePressed += OnContinuePressed;
            pausePopup.ResetPressed += OnResetPressed;
            pausePopup.QuitPressed += OnQuitPressed;

            gameOverPopup.PlayAgainPressed += OnResetPressed;
            gameOverPopup.QuitPressed += OnQuitPressed;
        }

        private void OnDestroy()
        {
            pausePopup.ContinuePressed -= OnContinuePressed;
            pausePopup.ResetPressed -= OnResetPressed;
            pausePopup.QuitPressed -= OnQuitPressed;

            gameOverPopup.PlayAgainPressed -= OnResetPressed;
            gameOverPopup.QuitPressed -= OnQuitPressed;
        }

        private void StartGame()
        {
            GameStarted?.Invoke();
            NewGame();
        }

        private void NewGame()
        {
            spawner.enabled = true;
            scoreText.text = gainedFruit.ToString();
        }
        public void onSliceFruit()
        {
            gainedFruit++;
            scoreText.text = gainedFruit.ToString();
        }

        public void Explode()
        {
            health--;
            spawner.enabled = false;
            HealthChanged?.Invoke(health);

            GameOverCheck();
        }

        private void Pause()
        {
            isPaused = !isPaused;
            if (isPaused)
            {
                fruit.Deactivate();
                pausePopup.Show();
            }

            else
            {
                fruit.Activate();
                pausePopup.Hide();
            }
        }

        private bool GameOverCheck()
        {
            if (health <= 0)
            {
                //bomb.OnMouseDown();
                fruit.Deactivate();
                currentState = GameState.GameOver;
                StartCoroutine(gameOverPopup.Show(gainedFruit));
                return true;
            }

            return false;
        }

        #region Button Event
        private void OnContinuePressed()
        {
            Pause();
        }

        private void OnResetPressed()
        {
            // reset all state
            currentState = GameState.Waiting;
            gainedFruit = 0;

            FruitChanged?.Invoke(gainedFruit);

            //gameOverPopup.ResetPopup();
        }

        private void OnQuitPressed()
        {
            Application.Quit();
        }
        #endregion

        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                Pause();
            }

            if (UnityEngine.Input.GetKeyDown(KeyCode.Space))
            {
                if (currentState == GameState.Waiting)
                {
                    currentState = GameState.Playing;
                    StartGame();
                }
            }

            if (isPaused || currentState != GameState.Playing) return;
        }
    }
}


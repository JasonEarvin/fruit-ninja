public enum GameState
{
    None,
    Waiting,
    Playing,
    GameOver
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja
{
    public class FruitScript : MonoBehaviour
    {
        public GameObject fruitPrefab;
        private bool isActive = true;
        public float startForce = 15f;
        Rigidbody rb;

        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody>();
            rb.AddForce(transform.up * startForce, ForceMode.Impulse);
        }

        public void Activate()
        {
            isActive = true;
        }

        public void Deactivate()
        {
            isActive = false;
        }

        private void OnMouseDown()
        {
            if (!isActive) return;
            FindObjectOfType<GameManager>().onSliceFruit();
            fruitPrefab.SetActive(false);
        }
    }
}


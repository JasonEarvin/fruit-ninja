using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FruitNinja.Input
{
    public class Button : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler
    {
        private Image _inputArea;
        public event Action ButtonDown;
        public event Action ButtonUp;
        public event Action ButtonExit;
        public event Action<bool, bool> StateChanged;

        private bool _active = true;
        private bool _pressed = false;

        protected bool Pressed => _pressed;

        //public Image InputArea => (_inputArea) ? _inputArea : (_inputArea = GetComponent<Image>());

        #region IPointerHandler
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            if (!_active) return;

            _pressed = true;
            ButtonDown?.Invoke();

            StateChanged?.Invoke(_active, _pressed);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if (!_active || !_pressed) return;
            if (eventData.dragging) return;

            _pressed = false;
            OnPointerUp();
            ButtonUp?.Invoke();

            StateChanged?.Invoke(_active, _pressed);
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            if (!_active) return;

            _pressed = false;
            ButtonExit?.Invoke();

            StateChanged?.Invoke(_active, _pressed);
        }
        #endregion

        protected virtual void OnPointerUp() { }

        public virtual void EnableInput()
        {
            _active = true;
            //InputArea.raycastTarget = _active; // DEBT inputArea bisa null kalau belum awake. check null?

            StateChanged?.Invoke(_active, _pressed);
        }

        public virtual void DisableInput()
        {
            _active = false;
            //InputArea.raycastTarget = _active;

            StateChanged?.Invoke(_active, _pressed);
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        protected virtual void OnDestroy()
        {
            ButtonDown = null;
            ButtonUp = null;
            ButtonExit = null;
        }
    }
}


using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

namespace FruitNinja
{
    public class GameOverPopupController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private Input.Button playAgainButton;
        [SerializeField] private Input.Button quitButton;

        [SerializeField] private Text finalScoreText;

        public event Action PlayAgainPressed;
        public event Action QuitPressed;

        private void Start()
        {
            playAgainButton.ButtonUp += OnPlayAgainPressed;
            quitButton.ButtonUp += OnQuitPressed;
        }

        private void OnDestroy() {
            playAgainButton.ButtonUp -= OnPlayAgainPressed;
            quitButton.ButtonUp -= OnQuitPressed;
        }

        private void OnPlayAgainPressed()
        {
            Hide();
            PlayAgainPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            Hide();
            QuitPressed?.Invoke();
        }

        private YieldInstruction delayStep = new WaitForSeconds(1f);
        public IEnumerator Show(int gainedFruit)
        {
            canvas.enabled = true;
            finalScoreText.text = gainedFruit.ToString();

            yield return delayStep;
            finalScoreText.gameObject.SetActive(true);
            yield return delayStep;
            playAgainButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);
        }

        public void Hide()
        {
            canvas.enabled = false;
        }

        public void ResetPopup()
        {
            finalScoreText.gameObject.SetActive(false);
            playAgainButton.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
        }
    }
}


using System;
using UnityEngine;

namespace FruitNinja
{
    public class PausePopupController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private Input.Button contiueButton;
        [SerializeField] private Input.Button resetButton;
        [SerializeField] private Input.Button quitButton;

        public event Action ContinuePressed;
        public event Action ResetPressed;
        public event Action QuitPressed;

        private void Start()
        {
            contiueButton.ButtonUp += OnContinuePressed;
            resetButton.ButtonUp += OnResetPressed;
            quitButton.ButtonUp += OnQuitPressed;
        }

        private void OnDestroy()
        {
            contiueButton.ButtonUp -= OnContinuePressed;
            resetButton.ButtonUp -= OnResetPressed;
            quitButton.ButtonUp -= OnQuitPressed;
        }

        // Update is called once per frame
        private void OnContinuePressed()
        {
            Hide();
            ContinuePressed?.Invoke();
        }

        private void OnResetPressed()
        {
            Hide();
            ResetPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            Hide();
            QuitPressed?.Invoke();
        }

        public void Show()
        {
            canvas.enabled = true;
            Time.timeScale = 0f;
        }

        public void Hide()
        {
            canvas.enabled = false;
            Time.timeScale = 1f;
        }
    }
}


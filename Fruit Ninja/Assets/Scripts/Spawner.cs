using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja
{
    public class Spawner : MonoBehaviour
    {
        public GameObject[] fruit;
        public Transform[] spawnPoints;

        public float minDelay = 0.1f;
        public float maxDelay = 1f;

        private void OnEnable()
        {
            StartCoroutine(SpawnFruits());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        IEnumerator SpawnFruits()
        {
            while (true)
            {
                GameObject prefab = fruit[Random.Range(0, fruit.Length)];

                float delay = Random.Range(minDelay, maxDelay);
                yield return new WaitForSeconds(delay);

                int spawnIndex = Random.Range(0, spawnPoints.Length);
                Transform spawnPoint = spawnPoints[spawnIndex];

                GameObject spawnedFruit = Instantiate(prefab, spawnPoint.position, spawnPoint.rotation);
                Destroy(spawnedFruit, 3f);
            }
        }
    }
}


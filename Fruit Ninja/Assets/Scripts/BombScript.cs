using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja
{
    public class BombScript : MonoBehaviour
    {
        public GameObject bombPrefab;
        public float startForce = 15f;
        Rigidbody rb;

        // Start is called before the first frame update
        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            rb.AddForce(transform.up * startForce, ForceMode.Impulse);
        }

        public void OnMouseDown()
        {
            FindObjectOfType<GameManager>().Explode();
            bombPrefab.SetActive(false);
        }
    }
}
